Light_Mail
==========

Email Wrapper originally designed for LightMVC. Requires PEAR Mail and Mail_Mime to be installed.

classes operate as a wrapper for these classes, enabling method chaining.

Sendmail Example:
<code><pre>
$mail = new Light_Mail();
$mail->from('dev@lightmvc.com')
  ->to( 'your_name@example.com' )
  ->subject( 'Example Subject' )
  ->message( '&lt;h1&gt;Hello World!&lt;/h1&gt;', 'html' )
  ->message( 'Hello World!', 'text' )
  ->send();
</code></pre>

SMTP Example:
<code><pre>
$mail = new Light_Mail_Smtp();
$mail->from('dev@lightmvc.com')
  ->to( 'your_name@example.com' )
  ->subject( 'Example Subject' )
  ->message( '&lt;h1&gt;Hello World!&lt;/h1&gt;', 'html' )
  ->message( 'Hello World!', 'text' )
  ->set_auth( true ) #authorization required?
  ->set_username( $username )
  ->set_password( $password )
  ->set_port( $port )
  ->set_hosts( $hosts )
  ->send();
</code></pre>
